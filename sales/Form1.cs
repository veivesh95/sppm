﻿using sales.class_files;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sales
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new buyer_stuffs.b_home().Show();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new sales_order_stuffs.s_home().Show();
        }
    }
}
