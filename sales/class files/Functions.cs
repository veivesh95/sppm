﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sales.class_files
{
    class Functions
    {
        public static SqlConnection con = ConnectionManager.GetConnection();

        public static String getValue(String table, String col, String choice, String wanted)
        {
            String query_ = "SELECT * FROM " + table + " WHERE " + col + "='" + choice + "'";
            String output = "";
            try
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(query_, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                output = dt.Rows[0][wanted].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                con.Close();
            }
            return output;
        }

        public static String getData(String table, String column, String search, String resultColumn)
        {
            String query_ = "SELECT * FROM " + table + " WHERE " + column + " ='" + search + "'";
            String output = "";

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(query_, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                output = dt.Rows[0][resultColumn].ToString();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return output;
        }

        public static void LoadComboBoxes(string query, ComboBox combo, String colName)
        {
            try
            {
                combo.Items.Clear();
                con.Open();
                SqlDataAdapter sda1 = new SqlDataAdapter(query, con);
                DataSet ds1 = new DataSet();
                sda1.Fill(ds1, "Table");
                DataTable dt1 = ds1.Tables["Table"];

                foreach (DataRow dr in dt1.Rows)
                {
                    combo.Items.Add(dr[colName].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }

        }

        public static DataTable showOrderedTableSales()
        {
            String query = "SELECT OrderId, OrderDate, OrderStatus, PaymentStatus, CustomerId, EmpId, TotalAmount FROM Orders WHERE OrderStatus = 'Confirmed' ORDER BY OrderDate ASC, OrderId ASC";
            DataTable data = new DataTable();
            try
            {
                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(query, con);
                adapter.Fill(data);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                con.Close();
            }
            return data;
        }

        public static DataTable showTableSales()
        {
            String query = "SELECT OrderId, OrderDate, OrderStatus, PaymentStatus, CustomerId, EmpId, TotalAmount FROM Orders";
            DataTable data = new DataTable();
            try
            {
                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(query, con);
                adapter.Fill(data);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                con.Close();
            }
            return data;
        }

        public static DataTable showTable(String table)
        {
            String query = "SELECT * FROM " + table;
            DataTable data = new DataTable();
            try
            {
                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(query, con);
                adapter.Fill(data);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                con.Close();
            }
            return data;
        }

        public static DataTable searchSalesTable(String searchColumn, String searchKey)
        {
            String query = "SELECT OrderId, OrderDate, OrderStatus, PaymentStatus, CustomerId, EmpId, TotalAmount FROM Orders WHERE " + searchColumn + " LIKE '%" + searchKey + "%'";
            DataTable data = new DataTable();
            try
            {
                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(query, con);
                adapter.Fill(data);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                con.Close();
            }
            return data;
        }

        public static void UpdateCancelledOrder(String orderId)
        {
            int exQuantity = 0, orderQuantity = 0;
            String product = "";

            String getOrderItemDetails = "SELECT ProductId, Quantity FROM OrderItem WHERE OrderId = '" + orderId + "'";
            String updateStatus = "UPDATE Orders SET OrderStatus = 'Cancelled' WHERE OrderId = '" + orderId + "'";
            String updateQuantity = "UPDATE Product SET QuantityAvailable = " + (exQuantity + orderQuantity) + " WHERE ProductId = '" + product + "'";

            try
            {
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(getOrderItemDetails, con);
                DataSet ds1 = new DataSet();
                sda.Fill(ds1, "Table");
                DataTable dt1 = ds1.Tables["Table"];

                foreach (DataRow dr in dt1.Rows)
                {
                    product = dr["ProductId"].ToString();
                    orderQuantity = Int32.Parse(dr["Quantity"].ToString());
                    exQuantity = Int32.Parse(Functions.getData("Product", "ProductId", product, "QuantityAvailable"));
                    updateQuantity = "UPDATE Product SET QuantityAvailable = " + (exQuantity + orderQuantity) + " WHERE ProductId = '" + product + "'";

                    Functions.ExecuteQuery2(updateQuantity);
                }
                Functions.ExecuteQuery2(updateStatus);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        public static DataTable searchTable(String table, String column, String searchKey)
        {
            String query = "SELECT * FROM " + table + " WHERE " + column + " LIKE '%" + searchKey + "%'";
            DataTable data = new DataTable();
            try
            {
                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(query, con);
                adapter.Fill(data);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                con.Close();
            }
            return data;
        }

        public static String getNextID(String col, String table, String prefix)
        {
            int id = 0;
            String query = "select max (substring(" + col + ", 4, len(" + col + "))) as id from " + table;
            try
            {
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(query, con);
                DataSet ds = new DataSet();
                sda.Fill(ds, "table");
                DataTable dt = ds.Tables["table"];
                foreach (DataRow dr in dt.Rows)
                {

                    if (dr["id"].ToString() == "")
                        id = 10000;
                    else
                        id = int.Parse(dr["id"].ToString()) + 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
            return prefix + id;
        }

        public static void ExecuteQuery(String query)
        {
            try
            {
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(query, con);
                sda.SelectCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                con.Close();
            }

        }
        public static void ExecuteQuery2(String query)
        {
            try
            {
                SqlDataAdapter sda = new SqlDataAdapter(query, con);
                sda.SelectCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public static void ClearAllText(Control con)
        {
            foreach (Control c in con.Controls)
            {
                if (c is TextBox)
                    ((TextBox)c).Clear();
                else
                    ClearAllText(c);
            }
        }

        public static void FillList(String table, String col, String orderTerm, ListBox list)
        {
            String query_ = "SELECT * FROM " + table + " ORDER BY " + orderTerm;
            try
            {
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(query_, con);
                DataSet ds1 = new DataSet();
                sda.Fill(ds1, "Table");
                DataTable dt1 = ds1.Tables["Table"];

                foreach (DataRow dr in dt1.Rows)
                {
                    string name = dr[col].ToString();
                    list.Items.Add(name);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                con.Close();
            }
        }

        public static DataTable GetListData(String table, String col, String listString)
        {
            String query_ = "SELECT * FROM " + table + " WHERE " + col + " = '" + listString + "'";
            DataTable result = null;

            try
            {
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(query_, con);
                DataSet ds1 = new DataSet();
                sda.Fill(ds1, "Table");
                DataTable dt1 = ds1.Tables["Table"];
                result = dt1;

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                con.Close();
            }

            return result;
        }

        //validation methods

        public static bool isText(string values)
        {
            Regex r = new Regex(@"^[a-zA-Z]+$");
            if (r.IsMatch(values))
                return true;
            return false;

        }

        public static bool isValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool isContact(String number)
        {
            Regex phoneNumpattern = new Regex(@"(?<!\d)\d{10}$");
            if (phoneNumpattern.IsMatch(number))
            {
                return true;
            }
            else
            {
                return false;
            }
        }




    }
}
