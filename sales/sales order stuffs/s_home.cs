﻿using sales.class_files;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;

namespace sales.sales_order_stuffs
{
    public partial class s_home : Form
    {
        private string tmpCustomer;
        private string tmpOrderId;

        public s_home()
        {
            InitializeComponent();
        }

        private void s_home_Load(object sender, EventArgs e)
        {
            orderPendingTable.DataSource = Functions.showOrderedTableSales().DefaultView;
            this.orderPendingTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            ordersTable.DataSource = Functions.showTableSales().DefaultView;
            this.ordersTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            tabControl1.SelectedIndex = 0;
            Functions.LoadComboBoxes("SELECT * FROM Customer", comboCustomer, "CustomerName");

            btnExport.Visible = false;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new sales_order_stuffs.s_add().Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void searchQuery_TextChanged(object sender, EventArgs e)
        {
            Functions.LoadComboBoxes("SELECT * FROM Customer", comboCustomer, "CustomerName");
            string SearchKey = searchQuery.Text;

            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage1"])
            {
                orderPendingTable.DataSource = Functions.searchSalesTable("OrderId", SearchKey);
            }
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage2"])
            {
                ordersTable.DataSource = Functions.searchSalesTable("OrderId", SearchKey);
            }
        }

        private void comboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            string choice = null;
            choice = comboCustomer.Text;

            this.tmpCustomer = Functions.getValue("Customer", "CustomerName", choice, "CustomerId");

            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage1"])
            {
                orderPendingTable.DataSource = Functions.searchSalesTable("CustomerId", tmpCustomer);
            }
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage2"])
            {
                orderPendingTable.DataSource = Functions.searchSalesTable("CustomerId", tmpCustomer);
            }
        }

        private void comboCustomer_MouseClick(object sender, MouseEventArgs e)
        {
            searchQuery.Clear();
        }

        private void orderPendingTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                DataGridViewRow row = this.orderPendingTable.Rows[e.RowIndex];
                this.tmpOrderId = row.Cells["OrderId"].Value.ToString();
            }

            DialogResult dialogResult = MessageBox.Show("Are you sure to cancel the order-" + tmpOrderId + " ?", "WARNING", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Functions.UpdateCancelledOrder(tmpOrderId);

            }
            else if (dialogResult == DialogResult.No)
            {
                Console.WriteLine("Cancellation cancelled");
            }
        }
        private void copyAlltoClipboard()
        {
            ordersTable.SelectAll();
            DataObject dataObj = ordersTable.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            copyAlltoClipboard();
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Microsoft.Office.Interop.Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            Microsoft.Office.Interop.Excel.Range CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage1"])
            {
                btnExport.Visible = false;
            }

            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage2"])
            {
                btnExport.Visible = true;
            }
        }
    }
}
